<?php

namespace Fluo\Sniffs\Naming;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\AbstractVariableSniff;
use PHP_CodeSniffer\Sniffs\Sniff;

final class FluoVariableNameSniff extends AbstractVariableSniff implements Sniff {

	/**
	 * @param File $phpcs_file
	 * @param int $token_pos
	 */
	protected function processMemberVar(File $phpcs_file, $token_pos): void {
		$tokens = $phpcs_file->getTokens();

		$member_properties = $phpcs_file->getMemberProperties($token_pos);
		if (empty($member_properties)) {
			return;
		}

		// Check if the class extends another class and get the name of the class that is extended.
		if (empty($tokens[$token_pos]['conditions']) === false) {
			$classPtr = key($tokens[$token_pos]['conditions']);
			$implementsNames = $phpcs_file->findImplementedInterfaceNames($classPtr);

			if ($implementsNames !== false && in_array('AnnotationInterface', $implementsNames) !== false) {
				return;
			}
		}

		// The name of a property must start with a lowercase letter, properties
		// with underscores are not allowed, except the cases handled above.
		$memberName = ltrim($tokens[$token_pos]['content'], '$');
		if (preg_match('/^[a-z]/', $memberName) === 1 && strpos($memberName, '_') === false) {
			return;
		}

		$error = 'Class property %s should use lowerCamel naming without underscores';
		$data = [$tokens[$token_pos]['content']];
		$phpcs_file->addError($error, $token_pos, 'LowerCamelName', $data);
	}

	/**
	 * @param File $phpcs_file
	 * @param int $token_pos
	 */
	protected function processVariable(File $phpcs_file, $token_pos): void {
		$tokens = $phpcs_file->getTokens();

		$varName = ltrim($tokens[$token_pos]['content'], '$');


		// If it's a php reserved var, then its ok.
		if (in_array($varName, $this->phpReservedVars)) {
			return;
		}

		// If it is a static public variable of a class, then it's ok.
		if ($tokens[($token_pos - 1)]['code'] === T_DOUBLE_COLON) {
			return;
		}

		if (preg_match('/^[a-z]+(?:_[a-z]+)*$/', $varName) === 0) {
			$error = "Variable \"$varName\" starts with a capital letter, but only \$snake_case is allowed";
			$fixable = $phpcs_file->addFixableError($error, $token_pos, 'SnakeVar');

			if ($fixable) {
				// Prepend all uppercase characters with an underscore.
				$varName = preg_replace('/(?<!^)[A-Z]/', '_$0', $varName);
				if ($varName) {
					$phpcs_file->fixer->beginChangeset();
					$phpcs_file->fixer->replaceToken($token_pos, '$' . strtolower($varName));
					$phpcs_file->fixer->endChangeset();
				}
			}
		}
	}

	/**
	 * @param File $phpcs_file
	 * @param int $token_pos
	 */
	protected function processVariableInString(File $phpcs_file, $token_pos): void {
		// Allow all types of variables inside strings.
		return;
	}
}
